module.exports = function(grunt) {

  // All configuration goes here
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // compass
    compass: {
      dist: {
        options: {
          sassDir: 'scss',
          cssDir: 'css',
          config: 'config.rb'
        }
      }
    },
    concat: {
      dist: {
        src: ['js/global/modernizr.min.js', 'js/global/libs/*.js', 'js/global/theme.js'],
        dest: 'js/scripts.js',
      }
    },
    // minify js
    uglify: {
      my_target: {
        files: [{
            'js/scripts.min.js': ['js/scripts.js']
        }]
      }
    },
    // compress images
    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: 'img-src/',
          src: ['**/*.{png,jpg,gif,svg}'],
          dest: 'img/',
        }]
      }
    },
    //watch
    watch: {
      sass: {
        files: 'scss/**/*.scss',
        tasks: ['compass'],
        options: {
          spawn: false
        }
      },
    },

  });

  // -- Load - Where we tell Grunt we plan to use this plug-in.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-compass');

  // -- Register - Where we tell Grunt what to do when we type "grunt" into the terminal.
  grunt.registerTask('default', ['imagemin', 'watch']);

  // -- Build production files js, img compression
  // grunt.registerTask('build', ['concat', 'uglify', 'imagemin']);

};