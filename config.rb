# Require any additional compass plugins here.
require 'breakpoint'
require 'compass'
require 'singularitygs'

# Toggle this between :development and :production when deploying the CSS to the
# live server. Development mode will retain comments and spacing from the
# original Sass source and adds line numbering comments for easier debugging.
#environment = :production
environment = :development

# In development, we can turn on the FireSass-compatible debug_info.
firesass = false
# firesass = true

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed
output_style = :compressed

disable_warnings = true

# Location of the your project's resources.
css_dir         = "css"
sass_dir        = "scss"
images_dir      = "img"
generated_images_dir = "img"
sprite_load_path = "scss/sprites"
javascripts_dir = "js"
preferred_syntax = :scss
# Set this to the root of your project. All resource locations above are
# considered to be relative to this path.
http_path = "/"

# Since Drupal themes can be installed in multiple locations, we don't need to worry about
# the absolute path to the theme from server root.
# To enable relative paths to assets via compass helper functions. Uncomment:
relative_assets = true

# Boolean Indicates whether line comments should be added to
# compiled css that says where the selectors were defined.
# Defaults to false in production mode, true in development mode
line_comments = false