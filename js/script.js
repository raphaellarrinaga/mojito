(function ($){
  Drupal.behaviors.mojito_theme = {
    attach: function (context) {
      // --------

      // Transitions Only After Page Load
      // credits: http://css-tricks.com/transitions-only-after-page-load/
      $("body").removeClass("preload");

      function replace_svg() {
        // give a fall back for svg images - http://toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script/
        if(!Modernizr.svg) {
          $('img[src$=".svg"]').attr('src', function() {
            return $(this).attr('src').replace('.svg', '.png');
          });
        }
      }

      // Init
      replace_svg();

      $(".top-nav").naver({
        maxWidth: "1019px",
      });

    // --------
    }
  }
})(jQuery);