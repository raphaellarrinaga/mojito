<?php
// kpr(get_defined_vars());
//kpr($theme_hook_suggestions);
//template naming
//page--[CONTENT TYPE].tpl.php
?>
<?php if( theme_get_setting('mothership_poorthemers_helper') ){ ?>
<!-- page.tpl.php-->
<?php } ?>

<?php print $mothership_poorthemers_helper; ?>

<div class="page">
  <header role="banner" class="site-header clearfix">
    <div class="g-container">

    <?php if($page['page_top']): ?>
      <?php print render($page['page_top']); ?>
    <?php endif; ?>

    <?php if ($logo): ?>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="header-logo">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
      </a>
    <?php endif; ?>

    </div>
  </header>

  <section class="content-wrapper g-container">

    <?php if ($page['sidebar_first']): ?>
    <div class="sidebar-first">
      <?php print render($page['sidebar_first']); ?>
    </div>
    <?php endif; ?>

    <main role="main" id="main-content">
      <?php if($messages){ ?>
        <div class="drupal-messages">
        <?php print $messages; ?>
        </div>
      <?php } ?>

      <?php print render($page['help']); ?>

      <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h1 class="page-title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>

      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>

      <?php if (isset($tabs['#primary'][0]) || isset($tabs['#secondary'][0])): ?>
        <nav class="tabs"><?php print render($tabs); ?></nav>
      <?php endif; ?>

      <?php print render($page['content_pre']); ?>

      <?php print render($page['content']); ?>

      <?php print render($page['content_post']); ?>

      <?php print render($page['page_bottom']); ?>

    </main><!-- /main-->

    <?php if ($page['sidebar_second']): ?>
      <div class="sidebar-second">
        <?php print render($page['sidebar_second']); ?>
      </div>
    <?php endif; ?>

  </section>

  <?php if ($page['footer']): ?>
  <footer class="site-footer" role="contentinfo">
    <div class="g-container">

    <?php print render($page['footer']); ?>

    </div>
  </footer>
  <?php endif; ?>


</div><!-- /page-->
