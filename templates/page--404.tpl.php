<?php
//kpr(get_defined_vars());
//kpr($theme_hook_suggestions);
//template naming
//page--[CONTENT TYPE].tpl.php
?>
<?php if( theme_get_setting('mothership_poorthemers_helper') ){ ?>
<!--node.tpl.php-->
<?php } ?>

<?php print $mothership_poorthemers_helper; ?>


<header role="banner" class="site-header clearfix">
  <?php if($page['header_left']): ?>
    <?php print render($page['header_left']); ?>
  <?php endif; ?>

  <?php if ($logo): ?>
    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="header-logo">
      <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
    </a>
  <?php endif; ?>

  <?php if($page['header_right']): ?>
    <?php print render($page['header_right']); ?>
  <?php endif; ?>
</header>

<div class="page l-container not-found">
  <div role="main" id="main-content">

    <article>
       <h1><?php print t('Ooops') ?></h1>
       <p><?php print t('Sorry, but the page you were trying to view does not exist. This can be the result of a mistyped address or an out-of-date link.') ?></p>
       <p><a href="<?php print $front_page; ?>"><?php print t('Home sweet home.') ?></a></p>
    </article>

  </div>
</div>