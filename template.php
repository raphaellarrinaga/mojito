<?php
/*
  Preprocess
*/

/*
function mojito_preprocess_html(&$vars) {
  //  kpr($vars['content']);

  global $theme;
  global $base_url;
  $path = drupal_get_path('theme', $theme);

  // http://realfavicongenerator.net/
  // Complete favicons set
  $vars['appletouchicon'] = '<link rel="apple-touch-icon" sizes="57x57" href="'. $base_url .'/'.  $path .'/favicons/apple-touch-icon-57x57.png">';
  $vars['appletouchicon'] .= '<link rel="apple-touch-icon" sizes="114x114" href="'. $base_url .'/'.  $path .'/apple-touch-icon-114x114.png">';
  $vars['appletouchicon'] .= '<link rel="apple-touch-icon" sizes="72x72" href="'. $base_url .'/'.  $path .'/apple-touch-icon-72x72.png">';
  $vars['appletouchicon'] .= '<link rel="apple-touch-icon" sizes="144x144" href="'. $base_url .'/'.  $path .'/apple-touch-icon-144x144.png">';
  $vars['appletouchicon'] .= '<link rel="apple-touch-icon" sizes="60x60" href="'. $base_url .'/'.  $path .'/favicons/apple-touch-icon-60x60.png">';
  $vars['appletouchicon'] .= '<link rel="apple-touch-icon" sizes="120x120" href="'. $base_url .'/'.  $path .'/favicons/apple-touch-icon-120x120.png">';
  $vars['appletouchicon'] .= '<link rel="apple-touch-icon" sizes="76x76" href="'. $base_url .'/'.  $path .'/favicons/apple-touch-icon-76x76.png">';
  $vars['appletouchicon'] .= '<link rel="apple-touch-icon" sizes="152x152" href="'. $base_url .'/'.  $path .'/favicons/apple-touch-icon-152x152.png">';
  $vars['appletouchicon'] .= '<link rel="icon" type="image/png" href="'. $base_url .'/'.  $path .'/favicons/favicon-196x196.png" sizes="196x196">';
  $vars['appletouchicon'] .= '<link rel="icon" type="image/png" href="'. $base_url .'/'.  $path .'/favicons/favicon-160x160.png" sizes="160x160">';
  $vars['appletouchicon'] .= '<link rel="icon" type="image/png" href="'. $base_url .'/'.  $path .'/favicons/favicon-96x96.png" sizes="96x96">';
  $vars['appletouchicon'] .= '<link rel="icon" type="image/png" href="'. $base_url .'/'.  $path .'/favicons/favicon-16x16.png" sizes="16x16">';
  $vars['appletouchicon'] .= '<link rel="icon" type="image/png" href="'. $base_url .'/'.  $path .'/favicons/favicon-32x32.png" sizes="32x32">';
  $vars['appletouchicon'] .= '<meta name="msapplication-TileColor" content="#93c7c6">';
  $vars['appletouchicon'] .= '<meta name="msapplication-TileImage" content="'. $base_url .'/'.  $path .'/favicons/mstile-144x144.png">';

  // Define all additionnal js files
  $options = array(
    'group' => JS_THEME,
  );
  drupal_add_js(drupal_get_path('theme', 'mojito'). '/js/libs/skrollr.min.js', $options);
}
*/

function mojito_preprocess_page(&$vars,$hook) {
  //typekit
  //drupal_add_js('http://use.typekit.com/XXX.js', 'external');
  //drupal_add_js('try{Typekit.load();}catch(e){}', array('type' => 'inline'));

  //webfont
  //drupal_add_css('http://cloud.webtype.com/css/CXXXX.css','external');

  //googlefont
  //  drupal_add_css('http://fonts.googleapis.com/css?family=Bree+Serif','external');

  // cloud.typography
   // drupal_add_css('//cloud.typography.com/6457712/802724/css/fonts.css','external');

  // Template for the 403 page
  $status = drupal_get_http_header("status");
  if($status == "403 Forbidden") {
    $vars['theme_hook_suggestions'][] = 'page__403';
  }
}

/*
function mojito_preprocess_region(&$vars,$hook) {
  //  kpr($vars['content']);
}
*/

function mojito_preprocess_block(&$vars, $hook) {
  //  kpr($vars['content']);

  //looks for unique block in a region $region-$blockcreator-$delta . can be used for blocks defined by modules
  $block = $vars['elements']['#block']->region . '-' . $vars['elements']['#block']->module . '-' . $vars['elements']['#block']->delta;

  // kpr($block);
  // print($block);

   switch ($block) {
     case 'header-menu_block-2':
       $vars['classes_array'][] = '';
       break;
     case 'sidebar-system-navigation':
       $vars['classes_array'][] = '';
       break;
    default:

    break;

   }

  // manipulate blocks per region
  switch ($vars['elements']['#block']->region) {
    case 'header':
      $vars['classes_array'][] = '';
      break;
    case 'sidebar':
      $vars['classes_array'][] = '';
      $vars['classes_array'][] = '';
      break;
    default:

      break;
  }

}

/*
function mojito_preprocess_node(&$vars,$hook) {
  //  kpr($vars['content']);

  // add a nodeblock
  // in .info define a region : regions[block_in_a_node] = block_in_a_node
  // in node.tpl  <?php if($noderegion){ ?> <?php print render($noderegion); ?><?php } ?>
  //$vars['block_in_a_node'] = block_get_blocks_by_region('block_in_a_node');
}
*/
/*
function mojito_preprocess_comment(&$vars,$hook) {
  //  kpr($vars['content']);
}
*/
/*
function mojito_preprocess_field(&$vars,$hook) {
  //  kpr($vars['content']);
  //add class to a specific field
  switch ($vars['element']['#field_name']) {
    case 'field_ROCK':
      $vars['classes_array'][] = 'classname1';
    case 'field_ROLL':
      $vars['classes_array'][] = 'classname1';
      $vars['classes_array'][] = 'classname2';
      $vars['classes_array'][] = 'classname1';
    case 'field_FOO':
      $vars['classes_array'][] = 'classname1';
    case 'field_BAR':
      $vars['classes_array'][] = 'classname1';
    default:
      break;
  }

}
*/
/*
function mojito_preprocess_maintenance_page(){
  //  kpr($vars['content']);
}
*/

/*
function mojito_form_alter(&$form, &$form_state, $form_id) {
  //if ($form_id == '') {
  // $form['submitted']['form_fields']['name']['#attributes']['placeholder'] = t("John Doe");
  //....
  //}
}
*/

/**
 * Changing markup of the language switcher + set a abbraviate version of the lang names
 * Source: https://www.drupal.org/node/1369090
 */
/*
function mojito_links__locale_block(&$vars) {
  foreach($vars['links'] as $language => $langInfo) {
    $abbr = $langInfo['language']->language;
    $name = $langInfo['language']->name;
    $vars['links'][$language]['title'] = '<abbr title="' . $name . '">' . $abbr . '</abbr>';
    $vars['links'][$language]['html'] = TRUE;
  }
  $content = theme_links($vars);
  return $content;
}
*/

/**
 * Exclude Stylesheets
 */
/*
function mojito_css_alter(&$css) {
  // Remove unneeded css files
  $path = drupal_get_path('module', 'responsive_dropdown_menus');
  unset($css[$path . '/theme/responsive-dropdown-menus.css']);
}
*/

/**
* Ensure Text areas are never ever resizeable EVER!
*/
function mojito_textarea($element) {
  $element['element']['#resizable'] = FALSE;
  return theme_textarea($element) ;
}